<?php
/**
 * Plugin Name:     Call To Action Flotante
 * Plugin URI:      http://jlq.clicker360.com/
 * Description:     Call to action y modal flotantes para suscripción a asesorías de JLQ
 * Author:          Jesus E. Franco Martinez <jefrancomix@gmail.com>
 * Author URI:      https://tzkmx.wordpress.com
 * Text Domain:     floating-cta
 * Domain Path:     /languages
 * Version:         0.1.0
 *
 * @package         Floating_Cta
 */


add_action('wp_head', function () {
    wp_enqueue_style( 'mkhb-font-awesome', get_template_directory_uri() . '/header-builder/admin/fonts/font-awesome.min.css', array(), THEME_VERSION, 'all' );
});

add_action('wp_footer', 'styles_and_scripts', 100);

function styles_and_scripts () {

    ?>
    <div id="floating-cta">
        <div class="cta-button">
        </div>
        <div class="social">
            <ul>
                <li>
                    <a href="https://www.facebook.com/JLQ63/">
                        <span class="vc_icon_element-icon fa fa-facebook"><em class="sr-only">José Luis Quintero en Facebook</em></span>
                    </a>
                </li>
                <li>
                    <a href="https://www.youtube.com/channel/UCJflxbDR68U4pJmcwn-pyow">
                        <span class="vc_icon_element-icon fa fa-youtube-play"><em class="sr-only">José Luis Quintero en Youtube</em></span>
                    </a>
                </li>
                <li>
                    <a href="https://www.linkedin.com/in/jose-luis-quintero-wellness/">
                        <span class="vc_icon_element-icon fa fa-linkedin"><em class="sr-only">José Luis Quintero en LinkedIn</em></span>
                    </a>
                </li>
                <li>
                    <a href="https://www.instagram.com/joseluisquinteroe/">
                        <span class="vc_icon_element-icon fa fa-instagram"><em class="sr-only">José Luis Quintero en Instagram</em></span>
                    </a>
                </li>
                <li>
                    <a href="https://twitter.com/jluisquinteroe">
                        <span class="vc_icon_element-icon fa fa-twitter"><em class="sr-only">José Luis Quintero en Twitter</em></span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <style>
        #floating-cta {
            position: fixed;
            right: 0;
            z-index: 999;
            top: 108px;

            font-weight: bold;
            padding: 0;
            text-align: right;
            -webkit-transition: all .2s ease-in;
            -moz-transition: all .2s ease-in;
            transition: all .2s ease-in;
        }
        #floating-cta .cta-button {
            /*width: 150px;*/
            height: 34px;
            background: url(<?= plugins_url( 'assets/asesorias.png', __FILE__ ) ?>) no-repeat;
            background-size: 100%;
            border-right: solid 10px #113c66;
        }
        #floating-cta .social {
            height: 30px;
            background: rgba(135, 136, 137, 0.8);
            color: whitesmoke;
            padding: 0 15px 0 0;
        }
        #floating-cta .social ul {
            list-style-type: none;
            margin-left: 0;
            display: flex;
            justify-content: space-around;
        }
        #floating-cta .social a {
            color: whitesmoke;
            font-size: 18px;
        }
        @media screen and (min-width:1200px) {
            #floating-cta {
                width: 260px;
                display: block;
            }
            #floating-cta .cta-button {
                height: 57px;
            }
            #floating-cta .social {
                padding-top: 10px;
                height: 34px;
            }
            #floating-cta .social a {
                font-size: 28px;
            }
        }
        @media screen and (max-width:480px) {
            #floating-cta {
                width: 120px;
                display: block;
            }
            #floating-cta .cta-button {
                border-right: none;
            }
            #floating-cta .social {
                display: none;
            }
        }
    </style>
    <div id="cta-modal-overlay-1" class="cta-modal-overlay">
        <div id="cta-modal-overclose-1" class="cta-modal-overclose"></div>
        <div id="cta-modal-window-1" class="cta-modal-window">
            <div id="cta-modal-close-1" class="mw-close-btn topRight image"></div>
            <?php echo do_shortcode('[contact-form-7 id=\'69\' title=\'Registro Modal\']'); ?>
        </div>
    </div>
    <style>
        #cta-modal-overlay-1 {
            z-index: 999999;
            background-color: rgba(0, 0, 0, 0.7);
        }
        .cta-modal-overlay {
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            position: fixed;
            cursor: default;
            display: none;
            width: 100%;
            height: 100%;
            overflow: auto;
        }
        #cta-modal-overclose-1 {
            z-index: 999999;
        }
        .cta-modal-overclose {
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            cursor: default;
            width: 100%;
            height: 100%;
        }
        .cta-modal-window {
            margin: auto;
            display: none;
        }
        #cta-modal-window-1 {
            width: 662px;
            padding: 0;
            border: 3px solid #979797;
            z-index: 999999;
            position: fixed;
            top: 10%;
            right: 0;
            left: 0;
            border-radius: 25px;
            height: auto;
            background: #113c66;
            font-family: inherit;
            font-size: 16px;
            box-shadow: 0 3px 4px #979797;
        }
        #cta-modal-close-1.mw-close-btn.image::before {
            content: "";
            text-align: center;
            width: 40px;
            height: 40px;
            line-height: 40px;
            color: #fff;
            font-family: inherit;
            font-size: 14px;
            font-weight: normal;
            font-style: normal;
            background: #000;
            border-radius: 25px;
            background-image: url(<?= esc_url( plugins_url('assets/X.png', __FILE__ ) ) ?>);
            display: block;
        }
        .mw-close-btn::before {
            white-space: nowrap;
            display: block;
            position: relative;
            transition: all .1s ease;
        }
        #cta-modal-close-1 {
            top: -15px;
            right: -15px;
        }
        .mw-close-btn, .mw-close-btn::before {
            transition: all .1s ease;
        }
        .mw-close-btn {
            cursor: pointer;
            position: absolute;
            display: none;
        }

        #cta-modal-window-1 label {
            color: white;
            font-weight: bold;
            line-height: 2.66em;
        }
        #cta-modal-window-1 input {
            border-radius: 8px;
            line-height: 1.6em;
        }
        .form-body {
            padding: 1em 3em 0;
            margin: 0.5em 0 1.5em 0;
        }
        .form-bottom {
            background: white;
            text-align: center;
            padding: 1em 0 0;
            border-radius: 0 0 1.2em 1.2em;
        }
        @media screen and (max-width:782px) {
            #cta-modal-window-1 {
                width: 480px;
                font-size: 14px;
                top: 5%;
            }
            #cta-modal-window-1 .form-body {
                padding: 0 2em;
                margin: 0.5em 0;
            }
            #cta-modal-window-1 input {
                line-height: 1.2em;
            }
            #cta-modal-window-1 label {
                line-height: 1.6em;
            }
            #cta-modal-window-1 .form-bottom {
                padding: 0.5em 0 0;
                display: flex;
                flex-direction: column;
            }
            .form-bottom input.wpcf7-submit {
                font-size: 12px;
                margin-left: 8px;
                margin-right: 8px;
            }
        }
        @media screen and (max-width:500px) {
            #cta-modal-window-1 {
                width: 280px;
                top: 3%;
            }
        }
    </style>
    <script>
;(function ($) {
    $(document).ready(function () {
        $('#floating-cta .cta-button').click(openModal)
        $('#cta-modal-close-1').click(closeModal)
        $('#cta-modal-overlay-1').click(closeModal)
    });

    function closeModal () {
        $('#cta-modal-overlay-1').hide()
        $('#cta-modal-overclose-1').hide()
        $('#cta-modal-close-1').hide()
        $('#cta-modal-window-1').hide()
    }
    function openModal () {
        $('#cta-modal-overlay-1').show()
        $('#cta-modal-overclose-1').show()
        $('#cta-modal-close-1').show()
        $('#cta-modal-window-1').show()
    }
})(jQuery)
    </script>
<?php }

